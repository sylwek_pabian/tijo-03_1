package com.company;

import java.util.Random;

class Vector {
    /**
     * funkcja zwraca wylosowany rozmiar wektora z zakresu <5, 10>
     */
    public static int generateSizeOfVector() {
        Random random = new Random();
        int bottomRange = 5;
        int topRsnge = 10;
        int sizeOfVector = random.nextInt(topRsnge - bottomRange +  1) + bottomRange;
        return sizeOfVector;
    }

    /**
     * funkcja wprowadza dane do wektora. Podczas przetwarzania danych nie
     * mozna korzystac z informacji o rozmiarze wektora. Prosze odpowiednio
     * obsluzyc wyjatek ArrayIndexOutOfBoundsException.
     * Podczas kazdej iteracji losujemy liczbe typu double a nastepnie wprowadzamy
     * ja do naszego wektora - do momentu az przekroczymy rozmiar tablicy i wystapi
     * wyjatek ArrayIndexOutOfBoundsException, ktory odpowiednio obsluzymy.
     */
    public static void insertDataToVector(double[] vector) {
        int i = 0;
        while (true) {
            try {
                Random rand = new Random();
                double d = rand.nextDouble();
                vector[i] = d;
                i++;
            } catch (ArrayIndexOutOfBoundsException e){
                System.out.println("Złapano wyjątek ArrayIndexOutOfBoundsException"/* + e*/);
                break;
            }
        }
    }

    /**
     * funkcja wyswietla zawartosc wektora w odpowiednim formacie. Podczas
     * przetwarzania danych nie mozna korzystac z
     * informacji o rozmiarze wektora. Prosze odpowiednio obsluzyc wyjatek
     * ArrayIndexOutOfBoundsException. Podczas kazdej iteracji wyswietlamy kolejny
     * rekord tablicy az przekroczymy jej rozmiar i wystapi wyjatek
     * ArrayIndexOutOfBoundsException, ktory odpowiednio obsluzymy.
     */
    public static void showVector(double[] vector) {
        int i = 0;
        while (true) {
            try {
                System.out.println("TAB[" + i + "]: " + vector[i]);
                i++;
            } catch (ArrayIndexOutOfBoundsException e){
                System.out.println("Złapano wyjątek ArrayIndexOutOfBoundsException" /*+ e*/);
                System.out.println("Koniec wyświetlania danych!");
                break;
            }
        }
    }

    /** funkcja, w ktorej bedziemy przeprowadzali testy nowych funkcjonalnosci. */
    public static void main(String[] args) {
        System.out.println();
        System.out.println("Generuje rozmiar wektora...");
        double[] vector = new double[generateSizeOfVector()];
        System.out.println("Wprowadzam dane do wektora...");
        insertDataToVector(vector);
        System.out.println("Wyswietlam zawartosc wektora");
        showVector(vector);
    }
}

